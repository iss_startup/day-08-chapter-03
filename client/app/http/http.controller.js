(function () {
    angular
        .module("sample")
        .controller("SignUpCtrl", ["$http", SignUpCtrl]);
    // Step 1 - Inject $http above

    function SignUpCtrl($http) {
        // Step 2 - Expect $http
        var self = this;

        self.signUpForm = {};
        self.successMessage = "Success: Thank you for registering!";
        self.errorMessage = "Error: Oops something bad happened. Please try again!";
        self.success;

        self.submit = function () {
            // Step 3: add code to submit data and display message
        };

        // Step 4: Open http.html and use submit

    }
})();